# ClearBank_API_Using_Serenity

## Decision Factor
- I did a POC on Serenity in my previous company and I found that it generates good report and also BDD can be easily implemented. (Though, I have not used BDD approach in this repo).

## Instructions
- Install Java JDK and set JAVA_HOME path in system environment variable
- Now add %JAVA_HOME%\bin in Path system environment variable
- Install Eclipse IDE for Java Developers from http://www.eclipse.org/downloads/eclipse-packages/ 
- Download maven from https://maven.apache.org/download.cgi => Binary zip archive and extract to any folder
- Set MAVEN_HOME in system environment variable with value as the folder in which maven is extracted from above step. Now add %MAVEN_HOME%\bin in Path system environment variable.
- Clone the repository
- Open cmd and run command "mvn clean verify serenity:aggregate" in the folder where repo was cloned.
- Once the build is comepleted, HTML report can be found within <cloned_repo>\target\site\serenity\index.html