package api_automation.test;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import api_automation.utils.ReuseableSpecifications;
import api_automation.utils.TestUtils;
import api_automation.testbase.TestBase;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Title;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SerenityRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClearBankTest extends TestBase {

	public static String deck_id;
	public static int cards_drawn = 0;

	@Title("Get a new deck of card")
	@Test
	public void test01() {
		deck_id = SerenityRest.given().spec(ReuseableSpecifications.getGenericRequestSpec()).param("deck_count", "1")
				.when().get("/new/shuffle/").then().spec(ReuseableSpecifications.getGenericResponseSpec()).extract()
				.path("deck_id");
	}

	@Title("Draw cards from the deck 5 times AND Draw between 1 and 5 cards from the deck each time")
	@Test
	public void test02() {
		String drawURL = deck_id + "/draw/";
		for (int i = 0; i < 5; i++) {
			int count = TestUtils.getRandomValue();
			cards_drawn += count;
			SerenityRest.given().spec(ReuseableSpecifications.getGenericRequestSpec()).param("count", count).when()
					.get(drawURL).then().spec(ReuseableSpecifications.getGenericResponseSpec());
		}

	}

	@Title("Verify that 52-cards_drawn remain in the deck at the end of the test")
	@Test
	public void test03() {

		SerenityRest.given().spec(ReuseableSpecifications.getGenericRequestSpec()).param("deck_count", "1").when()
				.get(deck_id).then().spec(ReuseableSpecifications.getGenericResponseSpec())
				.body("remaining", equalTo(52 - cards_drawn));
	}

}
